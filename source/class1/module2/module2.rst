Module 2 - Nginx API Management
===============================

.. warning :: In this lab, you can notice some cosmetic bugs in the Controller GUI. This is caused by UDF gateway and we don't have yet a fix. If an object is not deleted after clicking on Delete button, reload the page.

We will do the following labs:

.. toctree::
   :maxdepth: 1
   :glob:

   lab*

